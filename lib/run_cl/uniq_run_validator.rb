class UniqRunValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    run = Run.remove_format value

    matchers = []
    if record.new_record?
      matchers = record.class.where(attribute => run)
    else
      matchers = record.class.where('? = ? AND id != ?', attribute, run, record.id)
    end

    if options[:scope]
      matchers = matchers.where(options[:scope] => record.send(options[:scope]))
    end

    record.errors[attribute.to_sym] << (options[:message] || I18n.t(:uniq_run, scope: [:errors, :messages])) if matchers.any?
  end
end
