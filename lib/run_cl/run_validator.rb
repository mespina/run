class RunValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors[attribute] << (options[:message] || I18n.t(:run, scope: [:errors, :messages])) unless Run.valid?(value)
  end
end
