// The validator variable is a JSON Object
// The selector variable is a jQuery Object
window.ClientSideValidations.validators.local['run'] = function(element, options) {
  // Your validator code goes in here
  if (!/^(\d{1,3})\.{1}\d{3}\.{1}\d{3}-{1}(\d|k|K)$|^\d{7,9}\-{1}(\d|k|K)|^\d{7,9}(\d|k|K)$/i.test(element.val())) {
    // When the value fails to pass validation you need to return the error message.
    // It can be derived from validator.message
    return options.message;
  }
}