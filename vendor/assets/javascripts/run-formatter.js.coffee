$ ->
  window.format_run = (value) ->
    nPos  = 0
    sRut1 = value
    aRut  = sRut1.split('-')
    sRut1 = aRut.join('')
    aRut  = sRut1.split('.')
    sRut1 = aRut.join('')
    sInvertido = ""

    i = sRut1.length - 1

    while i >= 0
      sInvertido += sRut1.charAt(i)
      if i is sRut1.length - 1
        sInvertido += "-"
      else if nPos is 3
        sInvertido += "."
        nPos = 0
      nPos++
      i--

    j = sInvertido.length - 1
    sRut = ""

    while j >= 0
      unless sInvertido.charAt(sInvertido.length - 1) is "."
        sRut += sInvertido.charAt(j)
      else sRut += sInvertido.charAt(j)  unless j is sInvertido.length - 1
      j--

    sRut.toUpperCase()

  $('.rut').on 'blur', (event) ->
    event.target.value = format_run(event.target.value)

  window.unformat_run = (value) ->
    sRut = value
    aRut = sRut.split('-')
    sRut = aRut.join('')
    aRut = sRut.split('.')
    sRut = aRut.join('')

    sRut.toUpperCase()

  $('.rut').on 'focus', (event) ->
    event.target.value = unformat_run(event.target.value)

  $('.rut').trigger('blur')